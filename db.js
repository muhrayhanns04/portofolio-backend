const biodata = require("./data/biodata.json");
const skills = require("./data/skills.json");
const experiences = require("./data/experiences.json");
module.exports = {
	biodata,
	skills,
	experiences,
};
